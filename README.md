# Shorten URL Service
This is an abstraction on top of the CERN Shorten Url API (https://gitlab.cern.ch/webservices/web-redirector-v2/-/blob/master/app/api/README.md).

## Getting started
To use this library, you need to have an application registered on
Application Portal. This application need to be able to exchange token with the 
audience described on the Shorten URL API (https://auth.docs.cern.ch/user-documentation/oidc/exchange-for-api/).

Once your application has the right access to the API, you can use this library.

## Usage

1. Install using composer
```
composer-require glance-project/shorten-url-service
```

2. Start using to create your short URLs! You can follow the example bellow: :)
```php
use Glance\ShortenUrlService\ShortenUrl\Infrastructure\Provider\ShortenUrlProviderFactory;
$provider = ShortenUrlProviderFactory::createProviderFromCredentials(
    "<your-client-id>",
    "<your-client-secret>",
    (bool)"<in-production?>"
);
$shortUrl = $provider->createShortUrlFor("<target-url>");
```