<?php

declare(strict_types=1);

namespace Glance\ShortenUrlService\Authorization\Infrastructure\Web;

use Glance\ShortenUrlService\Authorization\Infrastructure\Web\Exception\UnableToFetchTokenException;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class AuthorizationApi
{
    private $client;
    private $clientId;
    private $clientSecret;
    private $inProduction;

    private const AUTHORIZATION_DOMAIN = "https://auth.cern.ch/auth/realms/cern/api-access/token";
    private const TEST_AUDIENCE = "web-redirector-v2-qa";
    private const PROD_AUDIENCE = "web-redirector-v2-production-cern-ch";

    private function __construct(
        Client $client,
        string $clientId,
        string $clientSecret,
        bool $inProduction
    ) {
        $this->client = $client;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->inProduction = $inProduction;
    }

    public static function createWithAppCredentials(
        Client $client,
        string $clientId,
        string $clientSecret,
        bool $inProduction = true
    ): self {
        return new self(
            $client,
            $clientId,
            $clientSecret,
            $inProduction
        );
    }

    private function formParams(): array
    {
        return [
            "grant_type" => "client_credentials",
            "client_id" => $this->clientId,
            "client_secret" => $this->clientSecret,
            "audience" => $this->inProduction ? self::PROD_AUDIENCE : self::TEST_AUDIENCE,
        ];
    }

    private function post(): ResponseInterface
    {
        $options = [
            "form_params" => array_merge($this->formParams()),
        ];
        try {
            return $this->client->post(
                self::AUTHORIZATION_DOMAIN,
                $options
            );
        } catch (\Throwable $th) {
            throw UnableToFetchTokenException::withCustomReason($th->getMessage());
        }
    }

    public function getAuthorizationToken(): ?string
    {
        $response = $this->post();
        $responseBody = json_decode((string) $response->getBody(), true);

        return $responseBody["access_token"];
    }
}