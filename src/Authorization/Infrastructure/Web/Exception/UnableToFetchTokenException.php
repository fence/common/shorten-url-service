<?php

declare(strict_types=1);

namespace Glance\ShortenUrlService\Authorization\Infrastructure\Web\Exception;

class UnableToFetchTokenException extends \Exception
{
    public static function withCustomReason(string $reason): self
    {
        return new self("Unable to fetch token using CERN SSO. Reason: {$reason}");
    }
}
