<?php

declare(strict_types=1);

namespace Glance\ShortenUrlService\ShortenUrl\Domain;

use DomainException;

class CernUrl
{
    protected $url;

    private function __construct(string $url)
    {    
        $this->url = $url;
    }

    public static function fromString(string $url)
    {
        self::assertHasProtocol($url);
        self::assertIsCernUrl($url);
        return new static($url);
    }

    public static function assertHasProtocol(string $url): void
    {      
        if (!(preg_match('~^https://~i', $url) || preg_match('~^http://~i', $url))) {
            throw new DomainException("The URL must start with 'https://'.");
        }
    }

    public static function assertIsCernUrl(string $url): void
    {
        $containsDotCern = strpos($url, '.cern');
        $domainIsCern = preg_match('~^https://cern.~i', $url) || preg_match('~^http://cern.~i', $url);

        if (!($containsDotCern || $domainIsCern)) {
            throw new DomainException("The URL is not a valid CERN URL: " . $url);
        }
    }

    public function toString(): string
    {
        return (string) $this->url;
    }
}
