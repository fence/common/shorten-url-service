<?php

declare(strict_types=1);

namespace Glance\ShortenUrlService\ShortenUrl\Application;

use Glance\ShortenUrlService\ShortenUrl\Infrastructure\Web\CernWebServicesShortenUrlApi;
use Glance\ShortenUrlService\ShortenUrl\Domain\CernUrl;

class CreateShortUrlHandler
{
    /** @var CernWebServicesShortenUrlApi */
    private $shortenUrlApi;

    /** @var string */
    private $authorizationToken;

    public function __construct(
        CernWebServicesShortenUrlApi $shortenUrlApi,
        string $authorizationToken
    ) {
        $this->shortenUrlApi = $shortenUrlApi;
        $this->authorizationToken = $authorizationToken;
    }

    public function handle(CreateShortUrlCommand $command): CernUrl
    {
        return $this->shortenUrlApi->createUrl(
            $command->targetUrl(),
            $this->authorizationToken
        );
    }
}
