<?php

declare(strict_types=1);

namespace Glance\ShortenUrlService\ShortenUrl\Application;

use Glance\ShortenUrlService\ShortenUrl\Domain\CernUrl;

class CreateShortUrlCommand
{
    /** @var CernUrl */
    private $targetUrl;

    public static function fromPrimitives(
        string $targetUrl
    ): self {
        $command = new self();

        $command->targetUrl = CernUrl::fromString($targetUrl);

        return $command;
    }

    public function targetUrl(): CernUrl
    {
        return $this->targetUrl;
    }
}
