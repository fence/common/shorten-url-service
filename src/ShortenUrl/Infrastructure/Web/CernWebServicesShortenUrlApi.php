<?php

declare(strict_types=1);

namespace Glance\ShortenUrlService\ShortenUrl\Infrastructure\Web;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Glance\ShortenUrlService\ShortenUrl\Infrastructure\Web\Exception\UnableToCreateShortenUrlException;
use Glance\ShortenUrlService\ShortenUrl\Domain\CernUrl;

/**
 * Adapter to the CERN Shorten URL API
 */
class CernWebServicesShortenUrlApi
{
    private const TEST_DOMAIN = "https://web-redirector-v2-qa.web.cern.ch/";
    private const PROD_DOMAIN = "https://web-redirector-v2-production-cern-ch.web.cern.ch/";

    private $client;
    private $inProduction;

    private function __construct(
        Client $client,
        bool $inProduction
    ) {
        $this->client = $client;
        $this->inProduction = $inProduction;
    }

    public static function createWithAppCredentials(
        Client $client,
        bool $inProduction
    ): self {
        return new self(
            $client,
            $inProduction
        );
    }

    private function post(string $domain, string $accessToken, array $data): ResponseInterface
    {
        $options = [
            "headers" => [
                "Content-Type" => "application/json",
                "Authorization" => "Bearer " . $accessToken
            ],
            "json" => $data
        ];

        return $this->client->post(
            $domain,
            $options
        );
    }

    public function createUrl(CernUrl $targetUrl, string $accessToken): CernUrl
    {
        $data = [
            "targetUrl" => $targetUrl->toString()
        ];

        $domain = $this->inProduction ? self::PROD_DOMAIN : self::TEST_DOMAIN;

        try {
            $response = $this->post(
                $domain . '_/api/shorturlentry/',
                $accessToken,
                $data
            );

            $responseBody = json_decode((string) $response->getBody(), true);

            return CernUrl::fromString(
                $responseBody["url"]
            );
        } catch (\Throwable $th) {
            throw UnableToCreateShortenUrlException::withCustomReason($th->getMessage());
        }
    }
}
