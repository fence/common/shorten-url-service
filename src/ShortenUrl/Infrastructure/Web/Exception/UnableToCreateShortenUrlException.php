<?php

declare(strict_types=1);

namespace Glance\ShortenUrlService\ShortenUrl\Infrastructure\Web\Exception;

class UnableToCreateShortenUrlException extends \Exception
{
    public static function withCustomReason(string $reason): self
    {
        return new self("Unable to create short URL using CERN API. Reason: {$reason}");
    }
}
