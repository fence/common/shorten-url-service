<?php

namespace Glance\ShortenUrlService\ShortenUrl\Infrastructure\Provider;

use DI\ContainerBuilder;
use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;

use Glance\ShortenUrlService\Authorization\Infrastructure\Web\AuthorizationApi;
use Glance\ShortenUrlService\ShortenUrl\Infrastructure\Web\CernWebServicesShortenUrlApi;
use Glance\ShortenUrlService\ShortenUrl\Application\CreateShortUrlHandler;
use Glance\ShortenUrlService\ShortenUrl\Infrastructure\Provider\ShortenUrlProvider;

abstract class ShortenUrlProviderFactory
{
    public static function createProviderFromCredentials(
        string $clientId,
        string $clientSecret,
        bool $inProduction
    ): ShortenUrlProvider {
        $definitions = [
            AuthorizationApi::class => function (Client $client) use ($clientId, $clientSecret, $inProduction) {
                return AuthorizationApi::createWithAppCredentials(
                    $client,
                    $clientId,
                    $clientSecret,
                    $inProduction
                );
            },
            CernWebServicesShortenUrlApi::class => function (Client $client) use ($inProduction) {
                return CernWebServicesShortenUrlApi::createWithAppCredentials(
                    $client,
                    $inProduction
                );
            },
            CreateShortUrlHandler::class => function (ContainerInterface $c) {
                return new CreateShortUrlHandler(
                    $c->get(CernWebServicesShortenUrlApi::class),
                    $c->get(AuthorizationApi::class)->getAuthorizationToken()
                );
            }
        ];
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions($definitions);
        $container = $containerBuilder->build();

        return new ShortenUrlProvider($container->get(CreateShortUrlHandler::class));
    }
}
