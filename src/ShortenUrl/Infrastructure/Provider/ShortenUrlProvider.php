<?php

namespace Glance\ShortenUrlService\ShortenUrl\Infrastructure\Provider;

use Glance\ShortenUrlService\ShortenUrl\Application\CreateShortUrlCommand;
use Glance\ShortenUrlService\ShortenUrl\Application\CreateShortUrlHandler;

class ShortenUrlProvider
{
    /** @var CreateShortUrlHandler */
    private $createShortUrlHandler;

    public function __construct(
        CreateShortUrlHandler $createShortUrlHandler
    ) {
        $this->createShortUrlHandler = $createShortUrlHandler;
    }

    public function createShortUrlFor(string $targetUrl): string
    {
        $command = CreateShortUrlCommand::fromPrimitives($targetUrl);
        return $this->createShortUrlHandler->handle($command)->toString();
    }
}
