<?php

declare(strict_types=1);

namespace Tests\Glance\ShortenUrlService\ShortenUrl\Infrastructure\Web;

use PHPUnit\Framework\TestCase;
use Glance\ShortenUrlService\ShortenUrl\Infrastructure\Web\CernWebServicesShortenUrlApi;
use Glance\ShortenUrlService\ShortenUrl\Domain\CernUrl;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class CernWebServicesShortenUrlApiTest extends TestCase
{
    /** @test */
    public function shouldPostToApiWithProperParameters(): void
    {
        $postTestDomain = "https://web-redirector-v2-qa.web.cern.ch/_/api/shorturlentry/";
        $accessToken = 'tpg-gives-access';
        $targetUrl = 'https://glance-staging.cern.ch/';
        $resultShortUrl = 'https://glance-staging.cern.ch/123';

        $expectedPostOptions = [
            "headers" => [
                "Content-Type" => "application/json",
                "Authorization" => "Bearer " . $accessToken
            ],
            "json" =>[
                "targetUrl" => $targetUrl
            ]
        ];

        $clientMock = $this->createMock(Client::class);

        $clientMock->expects($this->once())
            ->method('post')
            ->with(
                $postTestDomain,
                $expectedPostOptions
            )
            ->willReturn(new Response(200, [], json_encode(['url' => $resultShortUrl])));

        $cernApi = CernWebServicesShortenUrlApi::createWithAppCredentials($clientMock, false);

        $result = $cernApi->createUrl(CernUrl::fromString($targetUrl), $accessToken);

        $this->assertEquals(CernUrl::fromString($resultShortUrl), $result); 
    }
}
