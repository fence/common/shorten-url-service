<?php

declare(strict_types=1);

namespace Glance\ShortenUrlService\Tests\Unit;

use DomainException;
use PHPUnit\Framework\TestCase;
use Glance\ShortenUrlService\ShortenUrl\Domain\CernUrl;

final class CernUrlTest extends TestCase
{    
    /** @test */
    public function shouldAllowUrlWithHttp(): void
    {
        $url = 'http://glance-staging.cern.ch/';
        $cernUrl = CernUrl::fromString($url);
        $this->assertEquals($url, $cernUrl->toString());
    }

    /** @test */
    public function shouldAllowUrlWithHttps(): void
    {
        $url = 'https://glance-staging.cern.ch/';
        $cernUrl = CernUrl::fromString($url);
        $this->assertEquals($url, $cernUrl->toString());
    }

    /** @test */
    public function shouldAllowLongEncodedUrl(): void
    {
        $url = 'https://glance-staging.cern.ch/search?c%5B0%5D%5Bi%5D=0&c%5B0%5D%5Bp%5D=profession&c%5B0%5D%5Bo%5D=is&c%5B0%5D%5Bv%5D=5&c%5B1%5D%5Bi%5D=1&c%5B1%5D%5Bp%5D=affiliation&c%5B1%5D%5Bo%5D=is&c%5B1%5D%5Bv%5D=353&c%5B1%5D%5Bb%5D%5B%5D=0&c%5B1%5D%5Bb%5D%5B%5D=2&c%5B1%5D%5Bb%5D%5B%5D=7&c%5B2%5D%5Bi%5D=2&c%5B2%5D%5Bp%5D=appointment&c%5B2%5D%5Bo%5D=contains&c%5B2%5D%5Bv%5D=1271&c%5B3%5D%5Bi%5D=3&c%5B3%5D%5Bp%5D=pub_editors_count&c%5B3%5D%5Bo%5D=is&c%5B3%5D%5Bv%5D=5&c%5B3%5D%5Bb%5D%5B%5D=0&c%5B3%5D%5Bb%5D%5B%5D=2&c%5B3%5D%5Bb%5D%5B%5D=7&c%5B4%5D%5Bi%5D=4&c%5B4%5D%5Bp%5D=vetoed_countries&c%5B4%5D%5Bo%5D=contains&c%5B4%5D%5Bv%5D=43&c%5B4%5D%5Bb%5D%5B%5D=1&c%5B4%5D%5Bb%5D%5B%5D=3&c%5B5%5D%5Bi%5D=5&c%5B5%5D%5Bp%5D=priority&c%5B5%5D%5Bo%5D=contains&c%5B5%5D%5Bv%5D=1&c%5B5%5D%5Bb%5D%5B%5D=4&c%5B6%5D%5Bi%5D=6&c%5B6%5D%5Bp%5D=last_talk&c%5B6%5D%5Bo%5D=greaterthan&c%5B6%5D%5Bv%5D=2023-03-01&c%5B6%5D%5Bb%5D%5B%5D=5&c%5B7%5D%5Bi%5D=7&c%5B7%5D%5Bp%5D=profession&c%5B7%5D%5Bo%5D=is&c%5B7%5D%5Bv%5D=1&s%5B0%5D%5Bname%5D=score&s%5B0%5D%5Bdescending%5D=false';
        $cernUrl = CernUrl::fromString($url);
        $this->assertEquals($url, $cernUrl->toString());
    }

    /** @test */
    public function shouldAllowLongDecodedUrl(): void
    {
        $url = 'https://glance-staging.cern.ch/search?c[0][i]=0&c[0][p]=profession&c[0][o]=is&c[0][v]=5&c[1][i]=1&c[1][p]=affiliation&c[1][o]=is&c[1][v]=353&c[1][b][]=0&c[1][b][]=2&c[1][b][]=7&c[2][i]=2&c[2][p]=appointment&c[2][o]=contains&c[2][v]=1271&c[3][i]=3&c[3][p]=pub_editors_count&c[3][o]=is&c[3][v]=5&c[3][b][]=0&c[3][b][]=2&c[3][b][]=7&c[4][i]=4&c[4][p]=vetoed_countries&c[4][o]=contains&c[4][v]=43&c[4][b][]=1&c[4][b][]=3&c[5][i]=5&c[5][p]=priority&c[5][o]=contains&c[5][v]=1&c[5][b][]=4&c[6][i]=6&c[6][p]=last_talk&c[6][o]=greaterthan&c[6][v]=2023-03-01&c[6][b][]=5&c[7][i]=7&c[7][p]=profession&c[7][o]=is&c[7][v]=1&s[0][name]=score&s[0][descending]=false';
        $cernUrl = CernUrl::fromString($url);
        $this->assertEquals($url, $cernUrl->toString());
    }

    /** @test */
    public function shouldAllowWithCernInTheDomainWithHttps(): void
    {
        $url = "https://cern.ch/xxxx";
        $cernUrl = CernUrl::fromString($url);
        $this->assertEquals($url, $cernUrl->toString());
    }

    /** @test */
    public function shouldAllowWithCernInTheDomainWithHttp(): void
    {
        $url = "http://cern.ch/xxxx";
        $cernUrl = CernUrl::fromString($url);
        $this->assertEquals($url, $cernUrl->toString());
    }

    /** @test */
    public function shouldNotAllowUrlWithNoProtocol(): void
    {
        $this->expectException(DomainException::class);

        CernUrl::fromString('glance.cern.ch');
    }
    
    /** @test */
    public function shouldNotAllowOutsideDomain(): void
    {
        $this->expectException(DomainException::class);

        CernUrl::fromString('https://google.com.br');
    }

    /** @test */
    public function shouldNotAllowSpaces(): void
    {
        $this->expectException(DomainException::class);

        CernUrl::fromString(' https://glance-staging.cern.ch/');
    }

    /** @test */
    public function shouldNotAllowEmptyString(): void
    {
        $this->expectException(DomainException::class);

        CernUrl::fromString('');
    } 
}
