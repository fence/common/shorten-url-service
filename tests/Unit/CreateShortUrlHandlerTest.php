<?php

declare(strict_types=1);

namespace Glance\ShortenUrlService\Tests\Unit;

use PHPUnit\Framework\TestCase;

use Glance\ShortenUrlService\ShortenUrl\Infrastructure\Web\CernWebServicesShortenUrlApi;
use Glance\ShortenUrlService\ShortenUrl\Application\CreateShortUrlCommand;
use Glance\ShortenUrlService\ShortenUrl\Application\CreateShortUrlHandler;
use Glance\ShortenUrlService\ShortenUrl\Domain\CernUrl;

class CreateShortUrlHandlerTest extends TestCase
{
    /** @test */
    public function shouldCreateUrl(): void
    {
        $shortenUrlApi = $this->createMock(CernWebServicesShortenUrlApi::class);

        $targetUrl = 'https://glance-staging.cern.ch/atlas';
        $authorizationToken = 'tpg-gives-access';
        $shortenedUrl = CernUrl::fromString('https://glance-staging.cern.ch/123');

        $command = CreateShortUrlCommand::fromPrimitives($targetUrl);

        $shortenUrlApi->expects($this->once())
            ->method('createUrl')
            ->with(
                CernUrl::fromString($targetUrl),
                $authorizationToken
            )
            ->willReturn($shortenedUrl);

        $handler = new CreateShortUrlHandler($shortenUrlApi, $authorizationToken);

        $result = $handler->handle($command);

        $this->assertEquals($shortenedUrl, $result);
    }
}
